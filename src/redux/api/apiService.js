import axios from "axios";
import Cookies from "universal-cookie";

const BASE_URL = "https://be-shop.onrender.com/api";

const cookie = new Cookies();

export class ApiService {
  static apiService = axios.create({
    baseURL: BASE_URL,
    withCredentials: true,
    headers: {
      "Content-Type": "application/json",
    },
  });
  // Verify Email

  static authApiService = axios.create({
    baseURL: BASE_URL,
    withCredentials: true,
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${cookie.get("access_token")}`,
    },
  });

  // Product
  static async getAllProduct() {
    const response = await this.apiService.get("/products");
    return response.data;
  }

  static async deleteProductWithId(id) {
    const response = await this.authApiService.get(`/products/delete/${id}`);
    return response.data;
  }

  // Cart
  static async getAllCart() {
    const response = await this.authApiService.get("/carts/get_cart");
    return response.data.carts;
  }

  static async createCart(cart) {
    const response = await this.authApiService.post("/carts/create_cart", cart);
    return response.data.newCart;
  }

  static async updateCart({ cartID, quantity }) {
    const response = await this.authApiService.post("/carts/update_cart", {
      cartID,
      quantity,
    });

    return response.data.newCart;
  }

  static async removeCartItem(cartID) {
    const response = await this.authApiService.get(
      `/carts/remove_cart/${cartID}`
    );
    return response.data;
  }

  static async removeCartItemWithUserID() {
    const response = await this.authApiService.get(`/carts/remove_all_cart`);
    return response.data;
  }

  //Order

  static async createOrder(order) {
    const response = await this.authApiService.post(
      `/orders/create_order`,
      order
    );
    return response.data;
  }

  static async getAllOrder() {
    const response = await this.authApiService.get(`/orders/all_orders`);
    return response.data;
  }

  static async getAllOrderWithAdminRole() {
    const response = await this.authApiService.get(`/orders/all_orders_admin`);
    return response.data;
  }

  static async confirmOrder(orderNumber) {
    const response = await this.authApiService.get(
      `/orders/confirm/${orderNumber}`
    );
    return response.data;
  }
}
