import axios from "axios";
import Cookies from "universal-cookie";
import { accessTokenCookieOptions } from "../../utils/Config";

const BASE_URL = "https://be-shop.onrender.com/api/";

const cookie = new Cookies();

const authApi = axios.create({
  baseURL: BASE_URL,
  withCredentials: true,
  headers: {
    "Content-Type": "application/json",
    Authorization: `Bearer ${cookie.get("access_token")}`,
  },
});

authApi.interceptors.response.use(
  (response) => {
    return response;
  },
  async (error) => {
    const originalRequest = error.config;
    const errMessage = error.response.data.message;
    if (errMessage.includes("not logged in") && !originalRequest._retry) {
      originalRequest._retry = true;
      await refreshAccessTokenFn();
      return authApi(originalRequest);
    }
    return Promise.reject(error);
  }
);

export const refreshAccessTokenFn = async () => {
  const response = await axios.get(
    "https://be-shop.onrender.com/api/auth/refresh",
    {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${cookie.get("refresh_token")}`,
      },
    }
  );
  cookie.set(
    "access_token",
    response.data.access_token,
    accessTokenCookieOptions
  );

  cookie.set("logged_in", true, accessTokenCookieOptions);
  return response.data;
};

export const signUpUserFn = async (user) => {
  const response = await authApi.post("auth/register", user);
  return response.data;
};

export const loginUserFn = async (user) => {
  const response = await authApi.post("auth/login", user);
  return response.data;
};

export const verifyEmailFn = async (verificationCode) => {
  const response = await authApi.get(`auth/verifyemail/${verificationCode}`);
  return response.data;
};

export const logoutUserFn = async () => {
  cookie.remove("access_token");
  cookie.remove("refresh_token");
  cookie.remove("logged_in");
  const response = await authApi.get("auth/logout");
  return response.data;
};

export const getMeFn = async () => {
  const response = await authApi.get("users/me");
  return response.data;
};
