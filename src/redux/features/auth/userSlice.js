import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import {
  getNewAccessToken,
  getUserDetails,
  registerUser,
  userLogin,
  verifyEmail,
} from "./userAction";
import Cookies from "universal-cookie";
import {
  accessTokenCookieOptions,
  refreshTokenCookieOptions,
} from "../../../utils/Config";

const cookie = new Cookies();

const initialState = {
  loading: "idle",
  isLoading: false,
  userInfo: {}, // for user object
  access_token: null,
  // for storing the JWT
  isLoggin: false,
  error: null,
  success: false,
  verifyEmailSuccess: false,
  registerSuccess: false,
  message: "", // for monitoring the registration process.
};
export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    setRegisterSuccessToFalse: (state) => {
      state.registerSuccess = false;
    },
    autoLoggin: (state) => {
      if (cookie.get("logged_in")) {
        state.isLoggin = true;
      }
    },
    setUser: (state, action) => {
      state.userInfo = { ...action.payload };
    },
    logOut: () => initialState,
    resetVerifyEmailState: (state, action) => {
      state.verifyEmailSuccess = false;
      state.message = "";
    },
  },
  extraReducers: {
    //Register
    [registerUser.pending]: (state) => {
      state.loading = "pending";
      state.error = null;
      state.isLoading = true;
    },
    [registerUser.fulfilled]: (state, { payload }) => {
      state.loading = "idle";
      state.success = true;
      state.isLoading = false;
      state.registerSuccess = true; // registration successful
    },
    [registerUser.rejected]: (state, { payload }) => {
      state.loading = "idle";
      state.error = payload;
      state.isLoading = false;
    },

    //Login
    [userLogin.pending]: (state) => {
      state.loading = "pending";
      state.error = null;
      state.isLoading = true;
    },
    [userLogin.fulfilled]: (state, { payload }) => {
      state.loading = "idle";
      state.userInfo = payload.user;
      state.isLoading = false;
      state.isLoggin = true;
     
      state.access_token = payload.access_token;
    },
    [userLogin.rejected]: (state, { payload }) => {
      state.loading = "idle";
      state.isLoading = false;
      state.error = payload;
    },

    // Get user
    [getUserDetails.pending]: (state) => {
      state.loading = "pending";
    },
    [getUserDetails.fulfilled]: (state, { payload }) => {
      state.loading = "idle";
      state.userInfo = payload;
    },
    [getUserDetails.rejected]: (state, { payload }) => {
      state.loading = "idle";
      state.error = payload;
    },

    // Get new refresh token
    [getNewAccessToken.fulfilled]: (state, data) => {
      state.access_token = data.access_token;
    },

    //verify email

    [verifyEmail.pending]: (state, data) => {
      state.isLoading = true;
      state.loading = "pending";
    },
    [verifyEmail.fulfilled]: (state, { payload }) => {
      state.isLoading = false;
      state.message = payload.message;
      state.error = null;
      state.verifyEmailSuccess = true;
    },
    [verifyEmail.rejected]: (state, { payload }) => {
      state.isLoading = false;
      state.error = payload;
      state.verifyEmailSuccess = false;
    },
  },
});
export const {
  setRegisterSuccessToFalse,
  autoLoggin,
  logOut,
  setUser,
  resetVerifyEmailState,
} = userSlice.actions;

export default userSlice;
