import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

import Cookies from "universal-cookie";
import { ApiService } from "./../../api/apiService";

const cookie = new Cookies();

export const fetchAllCart = createAsyncThunk(
  "cart/fetchCart",
  async (arg, { getState, rejectWithValue, dispatch }) => {
    try {
      const cartItemsResponse = await ApiService.getAllCart();
      return cartItemsResponse;
    } catch (error) {
      console.log(error);
      if (error.response && error.response.data.message) {
        return rejectWithValue(error.response.data.message);
      } else {
        return rejectWithValue(error.message);
      }
    }
  }
);

export const addCartItem = createAsyncThunk(
  "cart/addCartItem",
  async (cartItem, thunkAPI) => {
    try {
      const data = await ApiService.createCart(cartItem);

      return data;
    } catch (error) {
      // return custom error message from API if any
      if (error.response && error.response.data.message) {
        return thunkAPI.rejectWithValue(error.response.data.message);
      } else {
        return thunkAPI.rejectWithValue(error.message);
      }
    }
  }
);

export const updateCart = createAsyncThunk(
  "cart/updateCart",
  async ({ cartID, quantity }, thunkAPI) => {
    try {
      const newCart = await ApiService.updateCart({ cartID, quantity });
      return newCart;
    } catch (error) {
      // return custom error message from API if any

      if (error.response && error.response.data.message) {
        return thunkAPI.rejectWithValue(error.response.data.message);
      } else {
        return thunkAPI.rejectWithValue(error.message);
      }
    }
  }
);

export const removeCartItem = createAsyncThunk(
  "cart/removeCart",
  async (id, { getState, dispatch, rejectWithValue }) => {
    try {
      await ApiService.removeCartItem(id);

      return id;
    } catch (error) {
      // return custom error message from API if any
      if (error.response && error.response.data.message) {
        return rejectWithValue(error.response.data.message);
      } else {
        return rejectWithValue(error.message);
      }
    }
  }
);

export const removeAllCartWithUserName = createAsyncThunk(
  "cart/removeAllCartWithUserName",
  async (arg, thunkAPI) => {
    try {
      const data = await ApiService.removeCartItemWithUserID();
      return data;
    } catch (error) {
      if (error.response && error.response.data.message) {
        return thunkAPI.rejectWithValue(error.response.data.message);
      } else {
        return thunkAPI.rejectWithValue(error.message);
      }
    }
  }
);
