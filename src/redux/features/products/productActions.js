import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import { multipartFormHeadersContentType } from "../../../utils/Config";
import { ApiService } from "./../../api/apiService";
import Cookies from "universal-cookie";

const cookie = new Cookies();

export const createProduct = createAsyncThunk(
  "productList/createProduct",
  async ({ values, imageFiles }, { rejectWithValue }) => {
    try {
      let formUpload = new FormData();

      formUpload.append("name", values.Name);
      formUpload.append("price", values.Price);
      formUpload.append("description", values.Description);
      formUpload.append("type", values.Type);
      formUpload.append("discount", values.Discount);

      for (let i = 0; i < imageFiles.length; i++) {
        console.log(imageFiles[i]);
        formUpload.append("files", imageFiles[i]);
      }

      const { data } = await axios.post(
        "https://be-shop.onrender.com/api/products/create_product",
        formUpload,
        {
          headers: {
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${cookie.get("access_token")}`,
          },
          withCredentials: true,
        }
      );

      return data;
    } catch (error) {
      if (error.response && error.response.data.message) {
        return rejectWithValue(error.response.data.message);
      } else {
        return rejectWithValue(error.message);
      }
    }
  }
);

export const updateProduct = createAsyncThunk(
  "productList/updateProduct",
  async ({ values, imageFiles, id, public_id_images }, { rejectWithValue }) => {
    try {
      let formUpload = new FormData();

      console.log(public_id_images);

      formUpload.append("name", values.Name);
      formUpload.append("price", values.Price);
      formUpload.append("description", values.Description);
      formUpload.append("type", values.Type);
      formUpload.append("discount", values.Discount);

      for (let i = 0; i < imageFiles.length; i++) {
        formUpload.append("files", imageFiles[i]);
      }
      for (let id of public_id_images) {
        formUpload.append("public_id_images", id);
      }

      const { data } = await axios.patch(
        `https://be-shop.onrender.com/api/products/update/${id}`,
        formUpload,
        {
          headers: {
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${cookie.get("access_token")}`,
          },
          withCredentials: true,
        }
      );

      return data;
    } catch (error) {
      if (error.response && error.response.data.message) {
        return rejectWithValue(error.response.data.message);
      } else {
        return rejectWithValue(error.message);
      }
    }
  }
);

export const deleteProduct = createAsyncThunk(
  "productList/deleteProduct",
  async (id, { rejectWithValue }) => {
    try {
      const data = await ApiService.deleteProductWithId(id);
      return id;
    } catch (error) {
      if (error.response && error.response.data.message) {
        return rejectWithValue(error.response.data.message);
      } else {
        return rejectWithValue(error.message);
      }
    }
  }
);
