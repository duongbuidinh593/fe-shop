import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import { createProduct, deleteProduct, updateProduct } from "./productActions";
import { ApiService } from "./../../api/apiService";

const initialState = {
  status: "loading",
  isLoading: false,
  isCreating: false,
  success: false,
  updatesuccess: false,
  products: [],
  error: null,
};

export const fetchAllProduct = createAsyncThunk(
  "productList/fetchAllProduct",
  async () => {
    const data = await ApiService.getAllProduct();
    return data;
  }
);

export const productListSlice = createSlice({
  name: "productList",
  initialState,
  reducers: {
    resetSuccess: (state) => {
      state.success = false;
    },
    resetUpdateSuccess: (state) => {
      state.updatesuccess = false;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchAllProduct.fulfilled, (state, action) => {
        state.products = action.payload;
        state.status = "finish";
      })
      .addCase(fetchAllProduct.pending, (state, action) => {
        state.status = "loading";
      })
      .addCase(createProduct.pending, (state, action) => {
        state.status = "creating";
        state.isCreating = true;
      })
      .addCase(createProduct.fulfilled, (state, { payload }) => {
        state.status = "finish";
        state.isCreating = false;
        state.products.push(payload.newProduct);
        state.success = true;
        state.error = null;
      })
      .addCase(createProduct.rejected, (state, { payload }) => {
        state.error = payload;
        state.isCreating = false;
      })
      .addCase(deleteProduct.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(deleteProduct.fulfilled, (state, { payload }) => {
        state.success = true;

        state.products.splice(
          state.products.findIndex((p) => p.id === +payload),
          1
        );

        state.isLoading = false;
        state.error = null;
      })
      .addCase(deleteProduct.rejected, (state, { payload }) => {
        state.error = payload;
        state.isLoading = false;
      })
      .addCase(updateProduct.fulfilled, (state, { payload }) => {
        console.log(payload);
        state.isLoading = false;
        state.error = null;
        state.updatesuccess = true;
        const idx = state.products.findIndex(
          (p) => p.id === +payload.newProduct.id
        );

        state.products[idx] = payload.newProduct;
      })
      .addCase(updateProduct.rejected, (state, { payload }) => {
        state.isLoading = false;
        state.error = payload;
        state.updatesuccess = false;
      })
      .addCase(updateProduct.pending, (state, { payload }) => {
        state.isLoading = true;
      });
  },
});

export const { resetSuccess, resetUpdateSuccess } = productListSlice.actions;
