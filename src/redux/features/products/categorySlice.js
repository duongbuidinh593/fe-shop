import { createSlice } from "@reduxjs/toolkit";
import { v4 as uuidv4 } from "uuid";

const listCategories = [
  {
    id: uuidv4(),
    type: "Stools",
    quantity: 16,
    url: "#",
    image_urls:
      "https://cdn.shopify.com/s/files/1/0279/3317/9952/files/cate1_600x_c154764e-f0e9-4166-a065-ee25a91b214b_600x.jpg?v=1572850149",
  },
  {
    id: uuidv4(),
    type: "Table",
    quantity: 16,
    url: "#",
    image_urls:
      "https://cdn.shopify.com/s/files/1/0279/3317/9952/files/cate3_600x_600x_90afc793-1e28-430a-bd8f-f3d7713f96a7_600x.jpg?v=1572850149",
  },
  {
    id: uuidv4(),
    type: "Desk Lamps",
    quantity: 16,
    url: "#",
    image_urls:
      "https://cdn.shopify.com/s/files/1/0279/3317/9952/files/cate2_600x_600x_bb761a52-553a-4a04-b4bb-b6d319fe3146_600x.jpg?v=1572850150",
  },
  {
    id: uuidv4(),
    type: "Wall Scones",
    quantity: 16,
    url: "#",
    image_urls:
      "https://cdn.shopify.com/s/files/1/0279/3317/9952/files/cate4_600x_600x_596a67f0-e6ed-4d93-ba8d-56c1fa1cd2fb_600x.jpg?v=1572850155",
  },
  {
    id: uuidv4(),
    type: "Living room",
    quantity: 16,
    url: "#",
    image_urls:
      "https://cdn.shopify.com/s/files/1/0279/3317/9952/files/cate5_600x_d4736af8-92be-41b9-8a23-5bfde96eddbe_600x.jpg?v=1572850149",
  },
];
export const categorySlice = createSlice({
  name: "category",
  initialState: listCategories,
  reducers: {},
});
