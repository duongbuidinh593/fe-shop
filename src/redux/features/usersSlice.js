import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  user: {
    name: "",
    email: "",
    role: "",
    _id: "",
    createdAt: "",
    updatedAt: "",
    __v: 0,
  },
};

export const usersSlice = createSlice({
  initialState,
  name: "usersSlice",
  reducers: {
    logout: () => initialState,
    setUser: (state, action) => {
      state.user = { ...action.payload };
    },
  },
});

export default usersSlice.reducer;

export const { logout, setUser } = usersSlice.actions;
