import { createSlice } from "@reduxjs/toolkit";
import { confirmOrder, getAllOrder } from "./orderAdminActions";

const initialState = {
  orders: [],
  status: "idle",
  isLoading: false,
  error: null,
};

export const orderAdminSlice = createSlice({
  name: "orderAdmin",
  initialState,
  reducers: {
    resetIsLoading: (state, actions) => {
      state.isLoading = false;
    },
  },
  extraReducers: {
    [getAllOrder.pending]: (state, { payload }) => {
      state.isLoading = true;
    },
    [getAllOrder.rejected]: (state, { payload }) => {
      state.error = payload;
      state.isLoading = false;
    },
    [getAllOrder.fulfilled]: (state, { payload }) => {
      state.orders = payload;
      state.isLoading = false;
    },
    [confirmOrder.fulfilled]: (state, { payload }) => {
      const idx = state.orders.findIndex((o) => o.orderNumber == payload);
      console.log(payload);
      const order = state.orders.find((o) => o.orderNumber == payload);
      console.log(order);

      order.status = "confirmed";

      state.orders[idx] = order;
      state.error = null;
    },
    [confirmOrder.rejected]: (state, { payload }) => {
      state.error = payload;
    },
  },
});

export const { resetIsLoading } = orderAdminSlice.actions;
