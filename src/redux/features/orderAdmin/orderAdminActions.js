import { createAsyncThunk } from "@reduxjs/toolkit";
import { ApiService } from "../../api/apiService";

export const getAllOrder = createAsyncThunk(
  "orderAdmin/getAllOrder",
  async (arg, thunkAPI) => {
    try {
      const ordersResponse = await ApiService.getAllOrderWithAdminRole();
      return ordersResponse;
    } catch (error) {
      console.log(error);
      if (error.response && error.response.data.message) {
        return thunkAPI.rejectWithValue(error.response.data.message);
      } else {
        return thunkAPI.rejectWithValue(error.message);
      }
    }
  }
);

export const confirmOrder = createAsyncThunk(
  "orderAdmin/confirm",
  async ({ orderNumber }, thunkAPI) => {
    try {
      const ordersResponse = await ApiService.confirmOrder(orderNumber);
      return orderNumber;
    } catch (error) {
      if (error.response && error.response.data.message) {
        return thunkAPI.rejectWithValue(error.response.data.message);
      } else {
        return thunkAPI.rejectWithValue(error.message);
      }
    }
  }
);
