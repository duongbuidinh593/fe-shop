import { createAsyncThunk } from "@reduxjs/toolkit";
import Cookies from "universal-cookie";

import { handleOrderDetailRequest } from "./../../../utils/FormHandle";
import { removeAllCartWithUserName } from "./../cart/cartActions";
import { ApiService } from "./../../api/apiService";
import { getAllOrder } from "./../orderAdmin/orderAdminActions";

const cookie = new Cookies();

export const fetchAllOrder = createAsyncThunk(
  "order/fetchAllOrder",
  async (arg, thunkAPI) => {
    try {
      const ordersResponse = await ApiService.getAllOrder();
      return ordersResponse;
    } catch (error) {
      console.log(error);
      if (error.response && error.response.data.message) {
        return thunkAPI.rejectWithValue(error.response.data.message);
      } else {
        return thunkAPI.rejectWithValue(error.message);
      }
    }
  }
);

export const createOrder = createAsyncThunk(
  "order/createOrder",
  async ({ customer_name, comments, order_address, cartItems }, thunkAPI) => {
    try {
      // configure authorization header with user's token
      const order_details = handleOrderDetailRequest(cartItems);

      const createOrderResponse = await ApiService.createOrder({
        customer_name,
        comments,
        order_address,
        order_details,
      });
      thunkAPI.dispatch(removeAllCartWithUserName());
      thunkAPI.dispatch(fetchAllOrder());
      thunkAPI.dispatch(getAllOrder());
      return createOrderResponse.data;
    } catch (error) {
      console.log(error);
      if (error.response && error.response.data.message) {
        return thunkAPI.rejectWithValue(error.response.data.message);
      } else {
        return thunkAPI.rejectWithValue(error.message);
      }
    }
  }
);
