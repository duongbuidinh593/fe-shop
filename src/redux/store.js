import { configureStore } from "@reduxjs/toolkit";
import { productListSlice } from "./features/products/productListSlice";
import { categorySlice } from "./features/products/categorySlice";

import { userSlice } from "./features/auth/userSlice";
import { cartSlice } from "./features/cart/cartSlice";
import orderSlice from "./features/order/orderSlice";
import { orderAdminSlice } from "./features/orderAdmin/orderAdminSlice";

export const store = configureStore({
  reducer: {
    productList: productListSlice.reducer,
    category: categorySlice.reducer,
    cart: cartSlice.reducer,
    user: userSlice.reducer,
    order: orderSlice.reducer,
    orderAdmin: orderAdminSlice.reducer,
  },
});
