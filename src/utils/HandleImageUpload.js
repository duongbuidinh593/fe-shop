export const ImagesToByte = (imageArr) => {
  var reader = new FileReader();
  var fileByteArray = [];
  let ImageByte = imageArr.map((item) => {
    reader.readAsArrayBuffer(item);
    reader.onloadend = (evt) => {
      if (evt.target.readyState === FileReader.DONE) {
        const arrayBuffer = evt.target.result,
          array = new Uint8Array(arrayBuffer);
        for (const a of array) {
          fileByteArray.push(a);
        }
        console.log(fileByteArray.length);
      }
    };
  });
};
