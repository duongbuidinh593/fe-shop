import Cookies from "universal-cookie";
const cookie = new Cookies();
const access_token = cookie.get("access_token");

export const applicationJsonHeadersContentType = {
  header: {
    "Content-Type": "application/json",
    Authorization: `Bearer ${access_token}`,
  },
};

export const multipartFormHeadersContentType = {
  header: {
    "Content-Type": "multipart/form-data",
    Authorization: `Bearer ${access_token}`,
  },
};

export const accessTokenCookieOptions = {
  maxAge: 15 * 60,
  expires: new Date(Date.now() + 30 * 60),
  secure: true,
};

export const refreshTokenCookieOptions = {
  maxAge: 60 * 60,
  expires: new Date(Date.now() + 30 * 60),
  secure: true,
};
