export const handleOrderDetailRequest = (cartItems) => {
  return cartItems.map((item) => {
    return {
      product_id: item.product_id,
      quantity: item.quantity,
      total: item.quantity * item.price,
    };
  });
};
