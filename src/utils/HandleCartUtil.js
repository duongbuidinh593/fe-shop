import { addCartItem, updateCart } from "../redux/features/cart/cartActions";

export const checkItemExist = (cartItem, cartItems) => {
  return cartItems.find((c) => c.product_id == cartItem.product_id);
};

export const addToCartItem = (cartItem, isExistCartItem, dispatch) => {
  console.log(isExistCartItem);
  console.log(cartItem);
  if (isExistCartItem) {
    dispatch(
      updateCart({
        cartID: isExistCartItem.id,
        quantity: isExistCartItem.quantity + cartItem.quantity,
      })
    );
  } else {
    dispatch(addCartItem(cartItem));
  }
};
