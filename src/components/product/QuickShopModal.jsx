import React, { useState } from "react";
import { Container } from "@mui/material";
import style from "./QuickShopModal.module.css";
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/navigation";

// import required modules
import { Navigation, Pagination } from "swiper";
import { Link, useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import { useSelector, useDispatch } from "react-redux";
import { addToCartItem, checkItemExist } from "../../utils/HandleCartUtil";
import { Toaster, toast } from "react-hot-toast";

import Price from "./Price";
const QuickShopModal = ({ product }) => {
  const cart = useSelector((state) => state.cart).cartItems;
  const navigate = useNavigate();
  const user = useSelector((state) => state.user);
  const dispatch = useDispatch();
  const formik = useFormik({
    initialValues: {
      quantity: 1,
    },
    onSubmit: (values) => {
      if (!user.isLoggin) {
        navigate("/login");
        return;
      }
      const cartItem = {
        product_id: product.id,
        quantity: values.quantity,
      };
      const isExistCartItem = checkItemExist(cartItem, cart);
      toast.success(`${product.name} was added`);
      addToCartItem(cartItem, isExistCartItem, dispatch);
    },
  });
  let myTextDes = product.description.split("-");
  myTextDes.shift();
  myTextDes.shift();

  return (
    <>
      <div className={`${style.modalContainer} sm:min-h-[90%] `}>
        <div
          className={`${style.modalContent} sm:flex-col  md:flex-1 md:flex-row`}
        >
          <div className={`${style.slideProductImg} xl:w-96 md:w-80`}>
            <Swiper
              navigation={true}
              loop={true}
              spaceBetween={60}
              pagination={{
                clickable: true,
              }}
              modules={[Navigation, Pagination]}
            >
              <SwiperSlide>
                <img className={style.productImg} src={product.image_urls[0]} />
              </SwiperSlide>
              <SwiperSlide>
                <img className={style.productImg} src={product.image_urls[1]} />
              </SwiperSlide>
            </Swiper>
          </div>
          <div className={`${style.productInfo} xl:w-[480px] md:w-80`}>
            <div className={style.title}>
              <h2 className={style.productName}>{product.name}</h2>
              <Price price={product.price} discount={product.discount} />
            </div>
            <hr />
            <form className="pb-6" onSubmit={formik.handleSubmit}>
              <div className={style.quantityInput}>
                <input
                  type="number"
                  id="quantity"
                  name="quantity"
                  value={formik.values.quantity}
                  onChange={formik.handleChange}
                  min="1"
                />
                <button className={style.submitBtn} type="submit">
                  Add to cart
                </button>
              </div>
            </form>
            <div className={style.productDescription}>
              <p>
                A classic 5-panel hat with our United By Blue logo on the front
                and an adjustable strap to keep fit and secure. Made with
                recycled polyester and organic cotton mix.
              </p>
              <ul className={style.listDescription}>
                {myTextDes.map((text) => (
                  <li>{text}</li>
                ))}
                <li>
                  <Link
                    className="border-b-2 border-spacing-1 border-slate-700 border-opacity-50"
                    to={`/product/${product.id}`}
                  >
                    View more
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default QuickShopModal;
