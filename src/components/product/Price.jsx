import React from "react";

const Price = ({ discount, price }) => {
  if (discount == 0) {
    return <span>${price}</span>;
  }
  const discountPrice = price - Math.floor((price * discount) / 100);

  return (
    <>
      <span className="text-gray-400 text-[16px] line-through">${price}</span>

      <span className="inline-block text-[16px] font-semibold ml-1">
        ${discountPrice}
      </span>
    </>
  );
};

export default Price;
