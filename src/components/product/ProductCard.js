import { Container } from "@mui/material";
import React from "react";
import CardImage from "./CardImage";
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import { Pagination, Navigation } from "swiper";
import CardInfo from "./CardInfo";
import QuickShopModal from "./QuickShopModal";
import { Modal } from "@mui/material";

// import required modules

const ProductCard = ({ product }) => {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  return (
    <div style={{ maxWidth: "480px" }}>
      <CardImage product={product} handleOpen={handleOpen} />
      <CardInfo
        name={product.name}
        price={product.price}
        discount={product.discount}
      />
      <Modal open={open} onClose={handleClose}>
        <QuickShopModal product={product} />
      </Modal>
    </div>
  );
};

export default ProductCard;
