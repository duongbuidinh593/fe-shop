import { Container, Grid } from "@mui/material";
import React from "react";
import logo1 from "../../img/brand11.jpg";
import logo2 from "../../img/brand21.jpg";
import logo3 from "../../img/brand31.jpg";
import logo4 from "../../img/brand41.jpg";
import style from "./ListBrand.module.css";

const ListBrand = () => {
  const listBrandImg = [logo1, logo2, logo3, logo4];
  return (
    <Container sx={{ marginTop: "64px" }}>
      <div key="imgcontainer" className={style.ImgContainer}>
        {listBrandImg.map((brandImg) => (
          <div key={`imgBrand ${brandImg}`} className={style.imgBrand}>
            <img
              key={brandImg}
              src={brandImg}
              alt="anh"
              style={{ width: "100%" }}
            />
          </div>
        ))}
        <div key={"div2"} className={style.imgBrand}>
          <img key={logo2} src={logo2} alt="anh" style={{ width: "100%" }} />
        </div>
        <div key="div3" className={style.imgBrand}>
          <img key={logo1} src={logo1} alt="anh" style={{ width: "100%" }} />
        </div>
      </div>
    </Container>
  );
};

export default ListBrand;
