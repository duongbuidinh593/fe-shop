import React, { useState } from "react";
import { Container } from "@mui/material";
import CheckOutForm from "../Form/CheckOutForm";
import CheckoutItem from "../CheckOut/CheckoutItem";
import { useSelector } from "react-redux/es/exports";
import Loading from "./../Loading/Loading";
import TotalSummary from "../Cart/TotalSummary";
import { Link, useNavigate } from "react-router-dom";

const CheckoutPage = () => {
  const cartStore = useSelector((state) => state.cart);
  const user = useSelector((state) => state.user);
  const navigate = useNavigate();
  if (!user.isLoggin) {
    navigate("/login");
    return;
  }
  if (cartStore.status === "loading") {
    return <Loading />;
  }

  const cartItems = cartStore.cartItems;

  if (cartItems.length === 0) {
    return (
      <Container>
        <div className="md:pb-36 pt-20  flex flex-col items-center gap-9 ">
          <h1 className="text-5xl text-gray-700">Nothing Here</h1>
          <div className="md:w-80">
            <img
              src="https://res.cloudinary.com/duqtjopvf/image/upload/v1659955379/shop/empty-cart_zkqopr.svg"
              alt="empty cart"
            />
          </div>
          <Link
            className="hover:border-b-2 hover:border-b-emerald-500 hover:text-emerald-500"
            to={"/products"}
          >
            Continue Shopping
          </Link>
        </div>
      </Container>
    );
  }
  let subTotal = 0;
  let discount = 0;
  cartItems.forEach((item) => {
    subTotal += item.price * item.quantity;
    discount += Math.floor((item.price * item.quantity * item.discount) / 100);
  });

  return (
    <Container sx={{ marginBottom: "120px" }}>
      <div className="text-center text-5xl">
        <h1>Check Out</h1>
      </div>
      <div className="relative mx-auto max-w-screen-2xl mt-12">
        <div className="grid grid-cols-1 md:grid-cols-2 md:gap-x-7">
          <div className="py-12 bg-gray-50 md:py-24">
            <div className="max-w-lg px-4 mx-auto lg:px-8">
              <div className="flex items-center">
                <span className="w-10 h-10 bg-blue-900 rounded-full"></span>

                <h2 className="ml-4 font-medium">BambooYou</h2>
              </div>

              <div className="my-12 mb-2">
                <div className="flow-root">
                  <ul className="-my-4 divide-y max-h-80 overflow-y-auto divide-gray-200">
                    {cartItems.map((item) => (
                      <CheckoutItem item={item} />
                    ))}
                  </ul>
                </div>
              </div>
              <div className="mt-12">
                <p className="mt-1 text-[18px] text-gray-700">
                  For the purchase of:
                </p>
                <p className="text-2xl font-medium tracking-tight">
                  ${parseFloat(subTotal - discount).toFixed(2)}
                </p>
              </div>
            </div>
          </div>
          <CheckOutForm cartItems={cartItems} />
        </div>
      </div>
    </Container>
  );
};

export default CheckoutPage;
