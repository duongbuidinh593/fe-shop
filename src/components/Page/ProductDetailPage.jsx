import React from "react";

import { useParams } from "react-router-dom";
import { Container } from "@mui/material";
import ProductDetailCarosoul from "../ProductDetail/ProductDetailImages";
import ProductDetailInfo from "../ProductDetail/ProductDetailInfo";
import { useSelector } from "react-redux";
import CircularProgress from "@mui/material/CircularProgress";
import Box from "@mui/material/Box";
import Loading from "../Loading/Loading";

const ProductDetailPage = () => {
  let params = useParams();

  const productList = useSelector((state) => state.productList);

  if (productList.status === "loading") {
    return <Loading />;
  }

  const products = productList.products;
  const product = products.find((p) => p.id == params.productid);

  return (
    <Container>
      <div className="relative max-w-screen-xl px-4 py-8 mx-auto">
        <div className="grid items-start grid-cols-1 gap-8 md:grid-cols-2">
          <ProductDetailCarosoul product={product} />

          <ProductDetailInfo product={product} />
        </div>
      </div>
    </Container>
  );
};

export default ProductDetailPage;
