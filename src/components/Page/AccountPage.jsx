import { Container } from "@mui/system";
import React from "react";
import { Link } from "react-router-dom";
import OrderTable from "../OrderItem/OrderTable";

const AccountPage = () => {
  return (
    <Container>
      <div>
        <h1 className="text-[42px]">My Account</h1>
      </div>

      <div>
        <h2 className="text-3xl">Order History</h2>

        <div>
          <OrderTable />
        </div>
      </div>
    </Container>
  );
};

export default AccountPage;
