import { borderTop } from "@mui/system";
import React from "react";
import LoginForm from "../Form/LoginForm";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

export const LoginPage = () => {
  const user = useSelector((state) => state.user);
  const navigate = useNavigate();
  if (user.isLoggin) {
    navigate("/my_account");
    return;
  }
  return (
    <div style={{ borderTop: "1px solid rgba(0,0,0,0.1)" }}>
      <LoginForm />
    </div>
  );
};
