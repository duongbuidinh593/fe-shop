import { Box, Container, Typography } from "@mui/material";
import { styled } from "@mui/material/styles";
import { FormProvider, SubmitHandler, useForm } from "react-hook-form";
import { object, string, TypeOf } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import FormInput from "../Form/FormInput";
import { useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { LoadingButton as _LoadingButton } from "@mui/lab";

import { toast } from "react-hot-toast";
import { useDispatch, useSelector } from "react-redux";
import { verifyEmail } from "./../../redux/features/auth/userAction";
import { resetVerifyEmailState } from "../../redux/features/auth/userSlice";

const LoadingButton = styled(_LoadingButton)`
  padding: 0.6rem 0;
  background-color: rgba(0, 0, 0, 0.7);
  color: #f2f2f2;
  font-weight: 500;

  &:hover {
    background-color: rgba(0, 0, 0, 0.5);
    transform: translateY(-2px);
  }
`;

const verificationCodeSchema = object({
  verificationCode: string().min(1, "Verification code is required"),
});

const EmailVerificationPage = () => {
  const { verificationCode } = useParams();
  const dispatch = useDispatch();

  const methods = useForm({
    resolver: zodResolver(verificationCodeSchema),
  });

  // 👇 API Login Mutation
  const { isLoading, message, verifyEmailSuccess, error, loading } =
    useSelector((state) => state.user);

  const navigate = useNavigate();

  const {
    reset,
    handleSubmit,
    formState: { isSubmitSuccessful },
  } = methods;

  useEffect(() => {
    if (verificationCode) {
      reset({ verificationCode });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (verifyEmailSuccess) {
    }

    if (error) {
      console.log(error);
      toast.error(error);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loading]);

  const onSubmitHandler = ({ verificationCode }) => {
    dispatch(verifyEmail({ verificationCode })).then((value) => {
      if (value.type === "email/verify/fulfilled") {
        toast.success("verify email success");
        navigate("/login");
        reset();
        resetVerifyEmailState();
      } else if (value.type === "email/verify/rejected") {
        toast.error(value.payload);
      }
    });
  };

  return (
    <Container
      maxWidth={false}
      sx={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        minHeight: "100vh",
        backgroundImage:
          "linear-gradient(to right bottom, #f2f2f2, #e8e8f3, #dddef4, #cfd5f6, #bfcdf8, #afcffc, #9dd1ff, #88d3ff, #74deff, #62e8ff, #59f2fc, #5ffbf1)",
      }}
    >
      <Box
        sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "column",
        }}
      >
        <Typography
          textAlign="center"
          component="h1"
          sx={{
            color: "#000",
            fontWeight: 600,
            fontSize: { xs: "2rem", md: "3rem" },
            mb: 2,
            letterSpacing: 1,
          }}
        >
          Verify Email Address
        </Typography>

        <FormProvider {...methods}>
          <Box
            component="form"
            onSubmit={handleSubmit(onSubmitHandler)}
            noValidate
            autoComplete="off"
            maxWidth="27rem"
            width="100%"
            sx={{
              backgroundColor: "#e5e7eb",
              p: { xs: "1rem", sm: "2rem" },
              borderRadius: 2,
            }}
          >
            <FormInput name="verificationCode" label="Verification Code" />

            <LoadingButton
              variant="contained"
              sx={{ mt: 1 }}
              fullWidth
              disableElevation
              type="submit"
              loading={isLoading}
            >
              Verify Email
            </LoadingButton>
          </Box>
        </FormProvider>
      </Box>
    </Container>
  );
};

export default EmailVerificationPage;
