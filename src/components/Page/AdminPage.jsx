import React from "react";
import AdminNav from "../Admin/AdminNav";
import AdminHeader from "./../Admin/AdminHeader";

import { Outlet } from "react-router-dom";
import { useDispatch } from "react-redux";
import { fetchAllProduct } from "./../../redux/features/products/productListSlice";

const AdminPage = () => {
  const dispatch = useDispatch();
  dispatch(fetchAllProduct());
  return (
    <>
      <AdminHeader />
      <div className="flex pt-14 min-h-screen">
        <AdminNav />
        <Outlet />
      </div>
    </>
  );
};

export default AdminPage;
