import React, { useState } from "react";
import CartSummaryItem from "../Cart/CartSummaryItem";
import Shipping from "../Cart/Shipping";
import TotalSummary from "../Cart/TotalSummary";
import { v4 as uuidv4 } from "uuid";
import { useSelector } from "react-redux";
import Loading from "../Loading/Loading";
import { Link } from "react-router-dom";

const CartSumaryPage = () => {
  const cartStore = useSelector((state) => state.cart);
  if (cartStore.status === "loading" && cartStore.status === "removing") {
    return <Loading />;
  }
  const cartItems = cartStore.cartItems;
  let subTotal = 0;
  let discount = 0;
  cartItems.forEach((item) => {
    subTotal += item.price * item.quantity;
    discount += Math.floor((item.price * item.quantity * item.discount) / 100);
  });

  return (
    <div className="py-14 px-4 md:px-6 2xl:px-20 2xl:container 2xl:mx-auto">
      <div className="flex justify-start item-start space-y-2 flex-col ">
        <h1 className="text-3xl mb-6 text-center lg:text-4xl font-semibold  leading-7 lg:leading-9  text-gray-800">
          Your cart
        </h1>
      </div>
      {cartItems.length === 0 ? (
        <div className="w-[20%]  border-t-gray-300 mx-auto mt-4 flex flex-col items-center ">
          <img
            className="object-cover"
            alt="empty cart"
            src="https://res.cloudinary.com/duqtjopvf/image/upload/v1659955379/shop/empty-cart_zkqopr.svg"
          />
          <span className="text-[24px] font-medium text-gray-600 text-center block mt-3 mb-1">
            Cart is empty
          </span>
          <Link
            className="hover:border-b-2 text-[14px] text-gray-500 hover:border-b-gray-900 hover:text-gray-900"
            to={"/products"}
          >
            Continue Shopping
          </Link>
        </div>
      ) : (
        <div className="mt-10 flex flex-col xl:flex-row jusitfy-center items-stretch  w-full xl:space-x-8 space-y-4 md:space-y-6 xl:space-y-0">
          <div className="flex flex-col justify-start  items-start w-full space-y-4 md:space-y-6 xl:space-y-8">
            <div
              key={"adsadasd"}
              className="flex flex-col justify-start items-start bg-gray-50 px-4 py-4 md:py-6 md:p-6 xl:p-8 w-full"
            >
              {cartItems.map((cartItem) => (
                <CartSummaryItem
                  key={cartItem.id}
                  id={cartItem.id}
                  cartItem={cartItem}
                />
              ))}
            </div>
            <div className="flex justify-center md:flex-row flex-col items-stretch w-full space-y-4 md:space-y-0 md:space-x-6 xl:space-x-8">
              <TotalSummary discount={discount} subTotal={subTotal} />
              <Shipping />
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default CartSumaryPage;
