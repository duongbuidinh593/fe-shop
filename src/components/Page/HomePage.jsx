import React from "react";
import Category from "../Category/Category";
import DecorationSection from "../Decoration/DecorationSection";
import HeaderCarosoul from "../header/HeaderCarosoul";
import ListBrand from "../ListBrand/ListBrand";
import TrendingSlider from "../product/TrendingSlider";
import LabTabs from "../FeaturedProductTable/FeaturedProductTab";

const HomePage = () => {
  return (
    <>
      <HeaderCarosoul />
      <TrendingSlider />
      <Category />
      <DecorationSection />
      <LabTabs />
      <ListBrand />
    </>
  );
};

export default HomePage;
