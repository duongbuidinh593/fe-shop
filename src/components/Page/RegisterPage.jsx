import { borderTop } from "@mui/system";
import React from "react";
import RegisterForm from "../Form/RegisterForm";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
export const RegisterPage = () => {
  const user = useSelector((state) => state.user);
  const navigate = useNavigate();
  if (user.isLoggin) {
    navigate("/my_account");
    return;
  }
  return (
    <div style={{ borderTop: "1px solid rgba(0,0,0,0.1)" }}>
      <RegisterForm />
    </div>
  );
};
