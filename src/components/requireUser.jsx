import { Navigate, Outlet, useLocation } from "react-router-dom";

import FullScreenLoader from "./FullScreenLoader";
import { useDispatch, useSelector } from "react-redux";
import Cookies from "universal-cookie";
import { getUserDetails } from "../redux/features/auth/userAction";

const cookie = new Cookies();

const RequireUser = ({ allowedRoles }) => {
  const logged_in = cookie.get("logged_in");
  const location = useLocation();
  const dispatch = useDispatch();
  

  // const { isLoading, isFetching } = userApi.endpoints.getMe.useQuery(null, {
  //   skip: false,
  //   refetchOnMountOrArgChange: true,
  // });

  // const loading = isLoading || isFetching;

  // const user = userApi.endpoints.getMe.useQueryState(null, {
  //   selectFromResult: ({ data }) => data,
  // });

  const { userInfo, isLoading } = useSelector((state) => state.user);
  const user = userInfo.user;

  if (isLoading) {
    return <FullScreenLoader />;
  }

  return (logged_in || user) && allowedRoles.includes(user?.role) ? (
    <Outlet />
  ) : logged_in && user ? (
    <Navigate to="/unauthorized" state={{ from: location }} replace />
  ) : (
    <Navigate to="/login" state={{ from: location }} replace />
  );
};

export default RequireUser;
