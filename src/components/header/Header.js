import React from "react";
import Navigation from "../Navigation/Navigation";
import HeaderBottom from "./HeaderBottom";

const Header = () => {
  return (
    <div className="">
      <Navigation />
      <HeaderBottom />
    </div>
  );
};

export default Header;
