import { Container, Badge } from "@mui/material";
import React, { useState } from "react";
import logo from "../../img/logo.png";
import style from "./HeaderBottom.module.css";
import { AiOutlineSearch } from "react-icons/ai";

import { Link } from "react-router-dom";
import Cart from "../Cart/Cart";

const HeaderBottom = () => {
  //TODO: Need to add number cart item state

  return (
    <>
      <Container
        maxWidth="lg"
        sx={{
          display: "flex",
          height: 90,
          alignItems: "center",
          gap: 9,
          justifyContent: "space-between",
        }}
      >
        <div>
          <Link to="/">
            <img src={logo} alt="anh" />
          </Link>
        </div>
        <div>
          <ul className={style.navBottom}>
            <Link to="/">Home </Link>
            <Link to="/products">
              <a href="#">Product</a>
            </Link>
            <li>
              <a href="#">FAQ</a>
            </li>
          </ul>
        </div>
        <div className={style.navBottomIcon}>
          <Cart />
        </div>
      </Container>
    </>
  );
};

export default HeaderBottom;
