import {
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@mui/material";
import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

const OrderTable = () => {
  const order = useSelector((state) => state.order).orderItems.orders;

  const products = useSelector((state) => state.productList).products;

  if (!order || order?.length === 0) {
    return (
      <>
        <div className="pb-56 pt-6">
          <p className="text-[18  px]">You haven't placed any orders yet.</p>
          <Link
            className="inline-block text-sm tracking-wide underline underline-offset-4  hover:text-stone-600"
            to={"/products"}
          >
            Continue shopping
          </Link>
        </div>
      </>
    );
  }

  const ordersSummary = order.map((o) => {
    return o.orderDetails.map((odt) => {
      const productName = products.find((p) => p.id === odt.productId).name;
      return {
        productName: productName,
        quantity: odt.quantity,
        total: odt.total,
      };
    });
  });

  const totals = order.map((o) => {
    let total = 0;
    o.orderDetails.map((odt) => {
      total += odt.total;
    });
    return total;
  });

  return (
    <TableContainer
      component={Paper}
      sx={{ marginTop: "24px", marginBottom: "48px" }}
    >
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell align="left">Order Number</TableCell>
            <TableCell align="left">Customer Name</TableCell>
            <TableCell align="left">Address</TableCell>
            <TableCell align="left">Order Date</TableCell>
            <TableCell align="left">Status</TableCell>
            <TableCell>Order Summary</TableCell>
            <TableCell>Total</TableCell>
          </TableRow>
        </TableHead>

        <TableBody>
          {order.map((orderItem, i) => (
            <TableRow
              key={orderItem.name}
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell component="th" scope="row">
                #{orderItem.orderNumber}
              </TableCell>

              <TableCell>{orderItem.fullName}</TableCell>
              <TableCell>{orderItem.orderAddress}</TableCell>
              <TableCell>{orderItem.orderDate}</TableCell>
              <TableCell>{orderItem.status}</TableCell>
              <TableCell>
                <ul>
                  {ordersSummary[i].map((orderDetailsItem) => {
                    return (
                      <li>
                        {orderDetailsItem.quantity} x{" "}
                        {orderDetailsItem.productName}
                      </li>
                    );
                  })}
                </ul>
              </TableCell>
              <TableCell>$ {totals[i]}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default OrderTable;
