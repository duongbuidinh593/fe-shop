import style from "./ProductCategory.module.css";
import React from "react";

const ProductCategory = (props) => {
  //TODO: khong hardcode
  //TODO: router

  return (
    <div className={style.productCategoryContainer}>
      <a href="#" className={style.ProductCategoryImg}>
        <img src={props.image_urls} />
      </a>

      <div className={style.categoryInfo}>
        <h3 className={style.typeCategory}>{props.type}</h3>
        <h5 className={style.quantity}>{props.quantity} products</h5>
      </div>
    </div>
  );
};

export default ProductCategory;
