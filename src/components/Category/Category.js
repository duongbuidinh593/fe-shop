import { Container } from "@mui/material";

import React from "react";
import style from "./Category.module.css";
import { useSelector } from "react-redux";
import ProductCategory from "./ProductCategory";
const Category = () => {
  const listCategories = useSelector((state) => state.category);
  return (
    <Container>
      <div className={style.categoryList}>
        {listCategories.map((category) => (
          <ProductCategory
            key={category.id}
            id={category.id}
            type={category.type}
            quantity={category.quantity}
            url={category.url}
            image_urls={category.image_urls}
          />
        ))}
      </div>
    </Container>
  );
};

export default Category;
