import React from "react";

const FilterTypeItem = (props) => {
  const handleOnclick = (e) => {
    props.setProducts((prev) => {
      if (e.target.id === "All") {
        console.log(props.allProducts);
        return props.allProducts;
      }
      return props.allProducts.filter((p) => p.type === e.target.id);
    });
  };
  return (
    <div class="flex items-center">
      <input
        id={props.type}
        type="radio"
        name="type[toy]"
        productType={props.type}
        class="w-5 h-5 border-gray-300 rounded"
        onClick={handleOnclick}
      />

      <label for="toy" class="ml-3 text-sm font-medium">
        {props.type}
      </label>
    </div>
  );
};

export default FilterTypeItem;
