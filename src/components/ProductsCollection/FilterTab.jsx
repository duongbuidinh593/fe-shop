import React, { useState } from "react";
import FilterTypeItem from "./FilterTypeItem";

const Types = [
  "All",
  "Men's Clothing",
  "Jewelery",
  "Women's Clothing",
  "Electronics",
];

const FilterTab = ({ setProducts, allProducts }) => {
  const [isOpen, setisOpen] = useState(true);
  window.addEventListener("resize", () => {
    const desktopScreen = window.innerWidth < 768;
    setisOpen(!desktopScreen);
  });

  return (
    <div class="lg:sticky lg:top-4 mt-14">
      <details
        open={isOpen}
        class="overflow-hidden border border-gray-200 rounded"
      >
        <summary class="flex items-center justify-between px-5 py-3 bg-gray-100 lg:hidden">
          <span class="text-sm font-medium">Toggle Filters</span>

          <svg
            class="w-5 h-5"
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              stroke-linecap="round"
              stroke-linejoin="round"
              stroke-width="2"
              d="M4 6h16M4 12h16M4 18h16"
            />
          </svg>
        </summary>

        <form class="border-t border-gray-200 lg:border-t-0">
          <fieldset>
            <legend class="block w-full px-5 py-3 text-xs font-medium bg-gray-50">
              Type
            </legend>

            <div class="px-5 py-6 space-y-2">
              {Types.map((type) => (
                <FilterTypeItem
                  type={type}
                  key={type}
                  setProducts={setProducts}
                  
                  allProducts={allProducts}
                />
              ))}
            </div>
          </fieldset>

          {/* 
//TODO:Filter product 
*/}
        </form>
      </details>
    </div>
  );
};

export default FilterTab;
