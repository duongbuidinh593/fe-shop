import React from "react";
import ProductCard from "../product/ProductCard";

const CollectionProducts = ({ products }) => {
  return (
    <div className="lg:col-span-3">
      <div className="flex items-center justify-end">
        <div className="ml-4  rounded-md ">
          <label for="SortBy" className="mr-3">
            Sort By
          </label>

          <select
            name="SortBy"
            className="text-md text-center  py-3 text-[#333] bg-[#f4f4f4] rounded "
          >
            <option value="manual">Featured</option>
            <option value="title-asc">Alphabetically,A-Z</option>
            <option value="title-desc">Alphabetically,Z-A</option>
            <option value="price-asc">Price, Low-High</option>
            <option value="price-desc">Price, High-Low</option>
          </select>
        </div>
      </div>

      <div className="grid grid-cols-1 gap-x-4 gap-y-5 mt-3 sm:grid-cols-2 lg:grid-cols-4">
        {products.map((product) => (
          <ProductCard key={product.id} product={product} />
        ))}
      </div>
    </div>
  );
};

export default CollectionProducts;
