import * as React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { AiOutlineHeart } from "react-icons/ai";

export default function ProductDetailImages({ product }) {
  const [src, setSrc] = React.useState(product.image_urls[0]);

  const handleChange = (event) => {
    setSrc(event.target.src);
  };

  return (
    <div className="grid relative grid-cols-2 gap-4 md:grid-cols-1">
      <div className="">
        <img
          alt="Mobile Phone Stand"
          className="object-contain rounded-xl"
          src={src}
        />
      </div>

      <div className="flex gap-4 lg:mt-4">
        {product.image_urls.map((p, i) => (
          <div className="cursor-pointer w-36">
            <img
              onClick={handleChange}
              key={i}
              id={i}
              alt="url"
              className="object-cover rounded-xl "
              src={p}
            />
          </div>
        ))}
      </div>
      <span className="absolute top-4 left-[84%] rounded-full shadow-md  bg-white inline-block px-2 py-2">
        <Link to={`/`}>
          <AiOutlineHeart key={`wishlist`} />
        </Link>
      </span>
    </div>
  );
}
