import { Container, TextField, Alert } from "@mui/material";
import React, { useEffect, useState } from "react";
import * as yup from "yup";
import { useFormik } from "formik";
import style from "./LoginForm.module.css";
import { Link, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import toast, { Toaster } from "react-hot-toast";
import { setRegisterSuccessToFalse } from "../../redux/features/auth/userSlice";
import {
  getUserDetails,
  userLogin,
} from "../../redux/features/auth/userAction";
import { fetchAllCart } from "../../redux/features/cart/cartActions";
import { LoadingButton as _LoadingButton } from "@mui/lab";
import { styled } from "@mui/material/styles";
import { fetchAllOrder } from "../../redux/features/order/orderActions";

const schema = yup
  .object({
    Username: yup.string().required(),
    Password: yup.string().required().min(5),
  })
  .required();

const LoadingButton = styled(_LoadingButton)`
  padding: 0.6rem 0;
  background-color: #000;
  color: #f2f2f2;
  font-weight: 500;

  &:hover {
    background-color: rgba(0, 0, 0, 0.7);
    transform: translateY(-2px);
  }
`;
const LoginForm = () => {
  const navigate = useNavigate();
  const { loading, errors, isLoggin, isLoading, registerSuccess } = useSelector(
    (state) => state.user
  );
  const [isFalse, setIsFalse] = useState(false);
  const dispatch = useDispatch();

  const formik = useFormik({
    initialValues: {
      Username: "",
      Password: "",
    },
    validationSchema: schema,
    onSubmit: (values) => {
      let userReq = {
        email: values.Username,
        password: values.Password,
      };
      dispatch(userLogin(userReq)).then((data) => {
        if (data.type === "user/login/fulfilled") {
          setIsFalse(false);
          toast.success("You've successfully");
          dispatch(getUserDetails());
          dispatch(fetchAllCart());
          dispatch(fetchAllOrder());
          navigate("/");
        } else {
          setIsFalse(true);
        }
      });
    },
  });
  return (
    <Container>
      {registerSuccess && <Toaster />}
      <div className={style.CustomerLogin}>
        <div className={style.CustomerLogin__header}>
          <h1>Login</h1>
          {isFalse && (
            <Alert
              sx={{ marginBottom: "16px" }}
              severity="error"
              key="username_login_failed_alert"
            >
              The username or password is not correct!
            </Alert>
          )}
        </div>
        <form onSubmit={formik.handleSubmit}>
          {formik.errors.Username && formik.touched.Username && (
            <Alert
              sx={{ marginBottom: "16px" }}
              severity="error"
              key="username_login_alert"
            >
              {formik.errors.Username}
            </Alert>
          )}
          <TextField
            fullWidth
            label="Username"
            name="Username"
            key="Username"
            value={formik.values.Username}
            onChange={formik.handleChange}
            sx={{
              marginBottom: "12px",
            }}
          />
          {formik.errors.Password && formik.touched.Password && (
            <Alert
              sx={{ marginBottom: "16px" }}
              severity="error"
              key="password_login_alert"
            >
              {formik.errors.Password}
            </Alert>
          )}
          <TextField
            fullWidth
            label="Password"
            type="password"
            value={formik.values.Password}
            onChange={formik.handleChange}
            name="Password"
            key="password_login"
            sx={{
              marginBottom: "12px",
            }}
          />

          <LoadingButton
            variant="contained"
            sx={{ mt: 1 }}
            fullWidth
            disableElevation
            type="submit"
            loading={isLoading}
          >
            Login
          </LoadingButton>
        </form>
        <ul className={style.redirectBox}>
          <li>
            <Link className={style.link} to="/.">
              Forgot your password?
            </Link>
          </li>
          <li>
            <Link className={style.link} to="/register">
              Create account
            </Link>
          </li>
        </ul>
      </div>
    </Container>
  );
};

export default LoginForm;
