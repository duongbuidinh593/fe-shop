import { Container, TextField, Alert, AlertTitle } from "@mui/material";
import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { LoadingButton as _LoadingButton } from "@mui/lab";
import { styled } from "@mui/material/styles";
import { useFormik } from "formik";
import * as Yup from "yup";
import style from "./RegisterForm.module.css";
import { Link, useNavigate } from "react-router-dom";
import toast, { Toaster } from "react-hot-toast";
import { registerUser } from "../../redux/features/auth/userAction";
import { setRegisterSuccessToFalse } from "../../redux/features/auth/userSlice";

const FORBIDEN_ERR = "User with that email already exist";
const LoadingButton = styled(_LoadingButton)`
  padding: 0.6rem 0;
  background-color: #000;
  color: #f2f2f2;
  font-weight: 500;

  &:hover {
    background-color: rgba(0, 0, 0, 0.7);
    transform: translateY(-2px);
  }
`;
const RegisterForm = () => {
  const [err, setErr] = useState(false);
  const [errMsg, setErrMsg] = useState("");
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { loading, error, isLoggin, isLoading, registerSuccess } = useSelector(
    (state) => state.user
  );
  useEffect(() => {
    if (registerSuccess) {
      toast.success("User registered successfully");
      navigate("/verifyemail");
      dispatch(setRegisterSuccessToFalse());
    }

    if (error) {
      console.log(error);
      toast.error(error);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loading]);

  const formik = useFormik({
    initialValues: {
      Fullname: "",
      Email: "",
      Password: "",
      ConfirmPassword: "",
    },
    validationSchema: Yup.object({
      Fullname: Yup.string().min(2).max(15).required("Required!"),
      Email: Yup.string().email().required("Required!"),
      Password: Yup.string().min(8).required("Required!"),
      ConfirmPassword: Yup.string()
        .oneOf([Yup.ref("Password")])
        .required("Required!"),
    }),
    onSubmit: (values) => {
      dispatch(
        registerUser({
          name: values.Fullname,
          email: values.Email,
          password: values.Password,
          passwordConfirm: values.ConfirmPassword,
        })
      );
    },
  });

  return (
    <Container>
      <div className={style.CustomerLogin}>
        <div className={style.CustomerLogin__header}>
          <h1>Create Account</h1>
        </div>
        {err && (
          <Alert
            severity="error"
            sx={{
              marginBottom: "12px",
              textAlign: "left",
            }}
            onClose={() => {
              setErr(false);
            }}
          >
            <AlertTitle>Error</AlertTitle>
            {errMsg}
          </Alert>
        )}
        <form onSubmit={formik.handleSubmit}>
          {formik.errors.Fullname && formik.touched.Fullname && (
            <Alert
              sx={{ marginBottom: "16px" }}
              key="fullnamealert"
              severity="error"
            >
              {formik.errors.Fullname}
            </Alert>
          )}
          <TextField
            fullWidth
            key="fullname"
            label="Full name"
            name="Fullname"
            value={formik.values.Fullname}
            onChange={formik.handleChange}
            sx={{
              marginBottom: "12px",
            }}
          />

          {formik.errors.Email && formik.touched.Email && (
            <Alert
              sx={{ marginBottom: "16px" }}
              key="email_alert"
              severity="error"
            >
              {formik.errors.Email}
            </Alert>
          )}

          <TextField
            fullWidth
            label="Email"
            name="Email"
            key="email"
            value={formik.values.Email}
            onChange={formik.handleChange}
            sx={{
              marginBottom: "12px",
            }}
          />

          {formik.errors.Password && formik.touched.Password && (
            <Alert
              sx={{ marginBottom: "16px" }}
              key="password_alert"
              severity="error"
            >
              {formik.errors.Password}
            </Alert>
          )}

          <TextField
            fullWidth
            label="Password"
            type="password"
            key="password"
            value={formik.values.Password}
            onChange={formik.handleChange}
            name="Password"
            sx={{
              marginBottom: "12px",
            }}
          />

          {formik.errors.ConfirmPassword && formik.touched.ConfirmPassword && (
            <Alert
              sx={{ marginBottom: "16px" }}
              severity="error"
              key="confirmpassword_alert"
            >
              {formik.errors.ConfirmPassword}
            </Alert>
          )}
          <TextField
            fullWidth
            label="Confirm Password"
            type="password"
            key="confirmpassword"
            value={formik.values.ConfirmPassword}
            onChange={formik.handleChange}
            name="ConfirmPassword"
            sx={{
              marginBottom: "12px",
            }}
          />

          <LoadingButton
            variant="contained"
            sx={{ mt: 1 }}
            fullWidth
            disableElevation
            type="submit"
            loading={isLoading}
          >
            Create
          </LoadingButton>
        </form>
        <ul className={style.redirectBox}>
          <li>
            <Link className={style.link} to="/login">
              You already have account? Login
            </Link>
          </li>
        </ul>
      </div>
    </Container>
  );
};

export default RegisterForm;
