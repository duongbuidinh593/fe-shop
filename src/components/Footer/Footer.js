import React from "react";
import { Container } from "@mui/material";
import style from "./Footer.module.css";

const Footer = () => {
  return (
    <div>
      <div className={style.contentFooter}>
        <Container>
          <div className={style.contentFooterContainer}>
            <ul className={style.contentItem}>
              <h3 className={style.contentItemHeader}>About us</h3>
              <li>PO Box 12300 Collins Street,</li>
              <li>Victoria 9000</li>
              <li>(+00) 1234 5678 90</li>
              <li>email@2020.com</li>
            </ul>
            <ul className={style.contentItem}>
              <h3 className={style.contentItemHeader}>Customer</h3>
              <li>About us</li>
              <li>Brands</li>
              <li>Contact us</li>
              <li>FAQS</li>
              <li>Search</li>
            </ul>
            <ul className={style.contentItem}>
              <h3 className={style.contentItemHeader}>Product</h3>
              <li>Orders</li>
              <li>Downloads</li>
              <li>Addresses</li>
              <li>Account details</li>
            </ul>
            <ul className={style.contentItem}>
              <h3 className={style.contentItemHeader}>My account</h3>
              <li>The board,</li>
              <li>Accessories</li>
              <li>FAQs</li>
              <li>Terms & Conditions</li>
              <li>Wishlist</li>
            </ul>
            <ul className={style.contentItem}>
              <h3 className={style.contentItemHeader}>Contact</h3>
              <li>Phone number: 0337648556</li>
              <li>Emaik:duongbui@gmail.com</li>
              <li>FAQs</li>
            </ul>
          </div>
        </Container>
      </div>
    </div>
  );
};

export default Footer;
