import React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { Button, ButtonGroup, Container } from "@mui/material";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux/es/exports";
import Loading from "../Loading/Loading";

import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import useMediaQuery from "@mui/material/useMediaQuery";
import { useTheme } from "@mui/material/styles";
import { useDispatch } from "react-redux";
import { deleteProduct } from "../../redux/features/products/productActions";
import { useEffect } from "react";
import { toast } from "react-hot-toast";
import { resetSuccess } from "../../redux/features/products/productListSlice";
import {
  confirmOrder,
  getAllOrder,
} from "./../../redux/features/orderAdmin/orderAdminActions";

export default function OrderManagement() {
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down("md"));

  const order = useSelector((state) => state.orderAdmin).orders;
  const productList = useSelector((state) => state.productList);
  const dispatch = useDispatch();
  dispatch(getAllOrder());

  const products = productList.products;

  const ordersSummary = order.map((o) => {
    return o.orderDetails.map((odt) => {
      const productName = products.find((p) => p.id === odt.productId).name;
      return {
        productName: productName,
        quantity: odt.quantity,
        total: odt.total,
      };
    });
  });

  const totals = order.map((o) => {
    let total = 0;
    o.orderDetails.map((odt) => {
      total += odt.total;
    });
    return total;
  });

  const { orders, isLoading, error } = useSelector((state) => state.orderAdmin);

  if (productList.status === "loading") {
    return <Loading />;
  }

  return (
    <Container>
      <div className="py-12 ">
        <h1 className="text-4xl text-center">All Order</h1>
      </div>
      <div className="flex justify-end">
        <Link className="capitalize" to={"/admin/create_product"}>
          Create
        </Link>
      </div>
      <div className="my-8 shadow-md">
        <TableContainer
          component={Paper}
          sx={{ marginTop: "24px", marginBottom: "48px" }}
        >
          <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell align="left">Order Number</TableCell>
                <TableCell align="left">Customer Name</TableCell>
                <TableCell align="left">Address</TableCell>
                <TableCell align="left">Order Date</TableCell>
                <TableCell align="left">Status</TableCell>
                <TableCell>Order Summary</TableCell>
                <TableCell sx={{ width: "98px" }}>Total</TableCell>
                <TableCell>Confirm</TableCell>
              </TableRow>
            </TableHead>

            <TableBody>
              {order.map((orderItem, i) => (
                <TableRow
                  key={orderItem.name}
                  sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                >
                  <TableCell component="th" scope="row">
                    #{orderItem.orderNumber}
                  </TableCell>

                  <TableCell>{orderItem.fullName}</TableCell>
                  <TableCell>{orderItem.orderAddress}</TableCell>
                  <TableCell>{orderItem.orderDate}</TableCell>
                  <TableCell>{orderItem.status}</TableCell>
                  <TableCell>
                    <ul>
                      {ordersSummary[i].map((orderDetailsItem) => {
                        return (
                          <li>
                            {orderDetailsItem.quantity} x{" "}
                            {orderDetailsItem.productName}
                          </li>
                        );
                      })}
                    </ul>
                  </TableCell>
                  <TableCell>$ {totals[i]}</TableCell>
                  <TableCell>
                    {orderItem.status === "pending" ? (
                      <Button
                        key={`confirm ${orderItem.orderNumber}`}
                        onClick={() => {
                          dispatch(
                            confirmOrder({ orderNumber: orderItem.orderNumber })
                          );
                        }}
                      >
                        Confirm
                      </Button>
                    ) : (
                      <p>Confirmed</p>
                    )}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </div>
    </Container>
  );
}
