import React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { Button, ButtonGroup, Container } from "@mui/material";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux/es/exports";
import Loading from "../Loading/Loading";
import EditOutlinedIcon from "@mui/icons-material/EditOutlined";
import DeleteOutlineOutlinedIcon from "@mui/icons-material/DeleteOutlineOutlined";

import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import useMediaQuery from "@mui/material/useMediaQuery";
import { useTheme } from "@mui/material/styles";
import { useDispatch } from "react-redux";
import { deleteProduct } from "../../redux/features/products/productActions";
import { useEffect } from "react";
import { toast } from "react-hot-toast";
import { resetSuccess } from "../../redux/features/products/productListSlice";

export default function ProductManagement() {
  const [open, setOpen] = React.useState(false);
  const [productID, setProductID] = React.useState(-1);
  const dispatch = useDispatch();
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down("md"));

  const { success, isLoading, error } = useSelector(
    (state) => state.productList
  );
  useEffect(() => {
    if (success) {
      toast.success(`Product #${productID} was deleted!`);
      dispatch(resetSuccess());
    }

    if (error) {
      toast.error(error);
    }
  }, [success]);
  const handleRemove = () => {
    setOpen(false);
    dispatch(deleteProduct(productID));
  };

  const handleClose = () => {
    setOpen(false);
  };

  const productList = useSelector((state) => state.productList);
  if (productList.status === "loading") {
    return <Loading />;
  }

  const products = productList.products;
  return (
    <Container>
      <div className="py-12 ">
        <h1 className="text-4xl text-center">All product</h1>
      </div>
      <div className="flex justify-end">
        <Link className="capitalize" to={"/admin/create_product"}>
          Create
        </Link>
      </div>
      <div className="my-8 shadow-md">
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell>ProductID</TableCell>
                <TableCell>Name</TableCell>
                <TableCell>Price</TableCell>
                <TableCell>Type</TableCell>
                <TableCell>Description</TableCell>
                <TableCell>Discount</TableCell>
                <TableCell>Image</TableCell>
                <TableCell></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {products.map((p) => (
                <TableRow
                  key={p.id}
                  sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                >
                  <TableCell component="th" scope="p">
                    #{p.id}
                  </TableCell>
                  <TableCell>{p.name}</TableCell>
                  <TableCell>{p.price}</TableCell>
                  <TableCell>{p.type}</TableCell>
                  <TableCell sx={{ maxWidth: "420px" }}>
                    {p.description}
                  </TableCell>
                  <TableCell>{p.discount}%</TableCell>
                  <TableCell>
                    <img src={p.image_urls[0]} className="w-32 rounded-md" />
                  </TableCell>
                  <TableCell align="right">
                    <ButtonGroup
                      orientation="vertical"
                      aria-label="vertical contained button group"
                      variant="contained"
                      sx={{
                        gap: "3px",
                      }}
                    >
                      {/* <Button
                        startIcon={<EditOutlinedIcon />}
                        variant="contained"
                        color="success"
                        key={`${p.id} + 2`}
                        sx={{ textTransform: "capitalize" }}
                      > */}
                      <Link
                        key={`link${p.id}`}
                        to={`/admin/update_product/${p.id}`}
                        className="bg-[#0DC09F] text-center py-2 rounded-md text-white inline-block "
                      >
                        Edit
                      </Link>
                      {/* </Button> */}
                      <Button
                        color="error"
                        variant="contained"
                        key={`${p.id} + 1`}
                        sx={{ textTransform: "capitalize" }}
                        onClick={() => {
                          setOpen(true);
                          setProductID(p.id);
                        }}
                      >
                        Delete
                      </Button>
                    </ButtonGroup>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        <Dialog
          fullScreen={fullScreen}
          open={open}
          onClose={handleClose}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">
            {"Delete this product?"}
          </DialogTitle>
          <DialogContent>
            <DialogContentText>
              This product will be removed. Lorem ipsum dolor sit amet
              consectetur adipisicing elit. Alias animi minus laborum, obcaecati
              atque ut optio totam accusamus, adipisci similique repellat
              quisquam
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button autoFocus onClick={handleClose}>
              Disagree
            </Button>
            <Button onClick={handleRemove} autoFocus>
              Agree
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    </Container>
  );
}
