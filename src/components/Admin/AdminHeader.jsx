import { Avatar } from "@mui/material";
import React from "react";

const AdminHeader = () => {
  return (
    <div className=" bg-[#072448] fixed h-14 w-full z-50 top-0 right-0 flex items-center">
      <h1 className="ml-5 text-[20px] text-white">Admin Shop</h1>
      <Avatar sx={{ position: "absolute", right: 24 }}>A</Avatar>
    </div>
  );
};

export default AdminHeader;
