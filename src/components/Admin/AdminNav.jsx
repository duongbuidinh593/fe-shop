import React from "react";
import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import Typography from "@mui/material/Typography";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { Link } from "react-router-dom";
import Inventory2OutlinedIcon from "@mui/icons-material/Inventory2Outlined";
import AddShoppingCartOutlinedIcon from "@mui/icons-material/AddShoppingCartOutlined";
const AdminNav = () => {
  return (
    <div className="flex-initial w-72 shadow-xl">
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography>Product Management</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
            <ul>
              <li className="">
                <Link
                  key={"product_management"}
                  to="./product_management"
                  className="h-fit flex items-center"
                >
                  <Inventory2OutlinedIcon sx={{ marginRight: "9px" }} /> All
                  Product
                </Link>
              </li>
            </ul>
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2a-content"
          id="panel2a-header"
        >
          <Typography>Order</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
            <ul>
              <li className="">
                <Link
                  key={"product_management"}
                  to="./order_management"
                  className="h-fit flex items-center"
                >
                  <AddShoppingCartOutlinedIcon sx={{ marginRight: "9px" }} />{" "}
                  All Order
                </Link>
              </li>
            </ul>
          </Typography>
        </AccordionDetails>
      </Accordion>
    </div>
  );
};

export default AdminNav;
