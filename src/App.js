import "./App.css";
import { Outlet, useNavigate } from "react-router-dom";
import Footer from "./components/Footer/Footer";
import Header from "./components/header/Header";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  getNewAccessToken,
  getUserDetails,
} from "./redux/features/auth/userAction";

import { fetchAllProduct } from "./redux/features/products/productListSlice";
import { fetchAllCart } from "./redux/features/cart/cartActions";
import { Toaster } from "react-hot-toast";
import { fetchAllOrder } from "./redux/features/order/orderActions";
import axios from "axios";
import Cookies from "universal-cookie";
import { getAllOrder } from "./redux/features/orderAdmin/orderAdminActions";

const cookie = new Cookies();
function App() {
  let dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchAllProduct());
  }, []);

  const is_logged = cookie.get("logged_in");

  useEffect(() => {
    if (!is_logged) {
      dispatch(getNewAccessToken());
    }
  }, [dispatch]);
  useEffect(() => {
    if (is_logged) {
      dispatch(getUserDetails());
    }
  }, [is_logged]);

  useEffect(() => {
    if (is_logged) dispatch(fetchAllCart());
  }, []);
  useEffect(() => {
    dispatch(fetchAllOrder());
  }, []);

  useEffect(() => {
    if (is_logged) {
      dispatch(getAllOrder());
    }
  }, []);

  return (
    <>
      <Header key="header" />
      <Outlet key="outlet" />
      <Footer key="footer " />
    </>
  );
}

export default App;
